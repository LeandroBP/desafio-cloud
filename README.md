# Desafio Cloud Infraestrutura como código (IAC)

Esse projeto foi criado com a intenção de exemplificar a implementação de serviços na AWS, utlizando o Terraform da Hashcorp para disponibilização da Infraestrutura como código. 

## PROVEDOR

* AWS - Amazon Web Services

## TERRAFORM - O QUE É E COMO FUNCIONA

![Image of Terraform](images/terraform.png)

* O Terraform é uma ferramenta para construir, alterar e configurar uma infraestrutura de rede de forma segura e eficiente. Com ele é possível gerenciar serviços de nuvem bem conhecidos, bem como soluções internas personalizadas. Veja a lista completa de serviços de infraestrutura de nuvem suportados em: https://www.terraform.io/docs/providers/index.html.

Os arquivos de configuração do Terraform descrevem os componentes necessários para executar um único aplicativo ou todo o seu datacenter. Ele pode gerar um plano de execução descrevendo o que ele fará para atingir o estado desejado e, em seguida, ele pode executar as instruções para construir a infraestrutura descrita. Conforme a configuração muda, o Terraform é capaz de determinar o que mudou e criar planos de execução incrementais que podem ser aplicados.

O Terraform trata a infraestrutura como código e dessa forma você pode versioná-lo usando o Git, por exemplo, e ter um backup, fazer rollback em caso de problemas e fazer auditoria à medida que o tempo passa e as alterações vão sendo realizadas no seu ambiente.

O Terraform é desenvolvido e mantido pela empresa Hashicorp. Ele é gratuito com código fonte aberto e assim pode receber contribuições da comunidade no GitHub (https://github.com/hashicorp/terraform). Ele está disponível para download na página: https://www.terraform.io/downloads.html

Neste tutorial, será mostrado como instalar e usar o Terraform para criar um instância na AWS.

## INSTALANDO O TERRAFORM

```
sudo su
apt-get update
apt-get install -y unzip
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip -d /usr/bin/
```
### VERIFICANDO A VERSÃO INSTALADA

```
terraform -v
terraform --help
```

## USANDO O TERRAFORM

1) Crie uma conta Free Tier na Amazon https://aws.amazon.com/ siga as instruções das páginas: https://docs.aws.amazon.com/chime/latest/ag/aws-account.html e https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/free-tier-limits.html. Na criação da conta será necessário cadastrar um cartão de crédito, mas como você criará instâncias usando os recursos oferecidos pelo plano Free Tier, nada será cobrado se você não ultrapassar o limite para o uso dos recursos e tempo oferecidos e descritos no link anterior.

2) Após a criação da conta na AWS, acesse a interface CLI da Amazon na página: https://aws.amazon.com/cli/

3) Clique no nome do usuário (canto superior direito) e escolha a opção “Security Credentials“. Em seguida clique na opção “Access Keys (Access Key ID and Secret Access Key)” e clique no botão “New Access Key” para criar e visualizar o ID e o Secret da chave, conforme exemplo abaixo (https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html). A Access Key e Secret Key mostradas a seguir são apenas para exemplo. Elas são inválidas e você precisa trocar pelos dados reais gerados para sua conta.


#### EXPORTAÇÃO DE CHAVES FAKE PARA ENVIROMENTS DA SUA MAQUINA

```
export Access_Key_ID=suacesskeyaqui
export Secret_Access_Key=suasecretkeyaqui
```

4) Agora podemos clonar o repositório.

```
git clone git@gitlab.com:LeandroBP/desafio-cloud.git
```

5) Em seguida basta entrar no diretório

```
cd ~/desafio-cloud
```

E por ultimo executar mais três comandos.

1° Precisamos iniciar o terraform neste diretório:

```
terraform init

```
Saida esperada:

```
$ terraform init
 Initializing provider plugins...
 The following providers do not have any version constraints in configuration,
 so the latest version was installed.
 To prevent automatic upgrades to new major versions that may contain breaking
 changes, it is recommended to add version = "..." constraints to the
 corresponding provider blocks in configuration, with the constraint strings
 suggested below.
 * provider.aws: version = "~> 2.58"
 Terraform has been successfully initialized!
 You may now begin working with Terraform. Try running "terraform plan" to see
 any changes that are required for your infrastructure. All Terraform commands
 should now work.
 If you ever set or change modules or backend configuration for Terraform,
 rerun this command to reinitialize your working directory. If you forget, other
 commands will detect it and remind you to do so if necessary.
```

2° Executar o plan para apresentar tudo que será criado ou modificado na infraestrutura antes de aplicar as alterções.

``` 
terraform plan
```

Saida esperada:

```
$ terraform plan
 Refreshing Terraform state in-memory prior to plan...
 The refreshed state will be used to calculate this plan, but will not be
 persisted to local or remote state storage.
 ------------------------------------------------------------------------
 An execution plan has been generated and is shown below.
 Resource actions are indicated with the following symbols:
   + create
 Terraform will perform the following actions:
   + aws_autoscaling_group.this
       id:                                         <computed>
       arn:                                        <computed>
       availability_zones.#:                       <computed>
       default_cooldown:                           <computed>
       desired_capacity:                           <computed>
       enabled_metrics.#:                          "8"
       enabled_metrics.119681000:                  "GroupStandbyInstances"
       enabled_metrics.1940933563:                 "GroupTotalInstances"
       enabled_metrics.308948767:                  "GroupPendingInstances"
       enabled_metrics.3267518000:                 "GroupTerminatingInstances"
       enabled_metrics.3394537085:                 "GroupDesiredCapacity"
       enabled_metrics.3551801763:                 "GroupInServiceInstances"
       enabled_metrics.4118539418:                 "GroupMinSize"
       enabled_metrics.4136111317:                 "GroupMaxSize"
       force_delete:                               "true"
       health_check_grace_period:                  "300"
       health_check_type:                          "ELB"
       launch_configuration:                       "${aws_launch_configuration.this.name}"
       load_balancers.#:                           <computed>
       max_size:                                   "5"
       metrics_granularity:                        "1Minute"
       min_size:                                   "2"
       name:                                       "cloud-autoscaling"
       protect_from_scale_in:                      "false"
       service_linked_role_arn:                    <computed>
       target_group_arns.#:                        <computed>
       vpc_zone_identifier.#:                      <computed>
       wait_for_capacity_timeout:                  "10m"
   + aws_autoscaling_policy.scaledown
       id:                                         <computed>
       adjustment_type:                            "ChangeInCapacity"
       arn:                                        <computed>
       autoscaling_group_name:                     "cloud-autoscaling"
       cooldown:                                   "300"
       metric_aggregation_type:                    <computed>
       name:                                       "Scale Down Desafio Cloud"
       policy_type:                                "SimpleScaling"
       scaling_adjustment:                         "-1"
   + aws_autoscaling_policy.scaleup
       id:                                         <computed>
       adjustment_type:                            "ChangeInCapacity"
       arn:                                        <computed>
       autoscaling_group_name:                     "cloud-autoscaling"
       cooldown:                                   "300"
       metric_aggregation_type:                    <computed>
       name:                                       "Scale Up Desafio Cloud"
       policy_type:                                "SimpleScaling"
       scaling_adjustment:                         "1"
   + aws_cloudwatch_metric_alarm.down
       id:                                         <computed>
       actions_enabled:                            "true"
       alarm_actions.#:                            <computed>
       alarm_description:                          "Scales down an instance when CPU utilization is lesser than 50%"
       alarm_name:                                 "ALB Scale DOWN"
       arn:                                        <computed>
       comparison_operator:                        "LessThanOrEqualToThreshold"
       dimensions.%:                               "1"
       dimensions.AutoScalingGroupName:            "cloud-autoscaling"
       evaluate_low_sample_count_percentiles:      <computed>
       evaluation_periods:                         "1"
       metric_name:                                "CPUUtilization"
       namespace:                                  "AWS/EC2"
       period:                                     "60"
       statistic:                                  "Average"
       threshold:                                  "50"
       treat_missing_data:                         "missing"
   + aws_cloudwatch_metric_alarm.up
       id:                                         <computed>
       actions_enabled:                            "true"
       alarm_actions.#:                            <computed>
       alarm_description:                          "Scales up an instance when CPU utilization is greater than 80%"
       alarm_name:                                 "ALB Scale UP"
       arn:                                        <computed>
       comparison_operator:                        "GreaterThanOrEqualToThreshold"
       dimensions.%:                               "1"
       dimensions.AutoScalingGroupName:            "cloud-autoscaling"
       evaluate_low_sample_count_percentiles:      <computed>
       evaluation_periods:                         "1"
       metric_name:                                "CPUUtilization"
       namespace:                                  "AWS/EC2"
       period:                                     "60"
       statistic:                                  "Average"
       threshold:                                  "80"
       treat_missing_data:                         "missing"
   + aws_db_instance.web
       id:                                         <computed>
       address:                                    <computed>
       allocated_storage:                          "10"
       apply_immediately:                          <computed>
       arn:                                        <computed>
       auto_minor_version_upgrade:                 "true"
       availability_zone:                          "us-east-1a"
       backup_retention_period:                    <computed>
       backup_window:                              <computed>
       ca_cert_identifier:                         <computed>
       character_set_name:                         <computed>
       copy_tags_to_snapshot:                      "false"
       db_subnet_group_name:                       "${aws_db_subnet_group.default.id}"
       delete_automated_backups:                   "true"
       endpoint:                                   <computed>
       engine:                                     "mysql"
       engine_version:                             "5.7"
       hosted_zone_id:                             <computed>
       identifier:                                 <computed>
       identifier_prefix:                          <computed>
       instance_class:                             "db.t2.micro"
       kms_key_id:                                 <computed>
       license_model:                              <computed>
       maintenance_window:                         <computed>
       monitoring_interval:                        "0"
       monitoring_role_arn:                        <computed>
       multi_az:                                   <computed>
       name:                                       "cloud"
       option_group_name:                          <computed>
       parameter_group_name:                       "default.mysql5.7"
       password:                                   <sensitive>
       performance_insights_enabled:               "false"
       performance_insights_kms_key_id:            <computed>
       performance_insights_retention_period:      <computed>
       port:                                       <computed>
       publicly_accessible:                        "false"
       replicas.#:                                 <computed>
       resource_id:                                <computed>
       skip_final_snapshot:                        "true"
       status:                                     <computed>
       storage_type:                               "gp2"
       timezone:                                   <computed>
       username:                                   "usuarioitau"
       vpc_security_group_ids.#:                   <computed>
   + aws_db_subnet_group.default
       id:                                         <computed>
       arn:                                        <computed>
       description:                                "Managed by Terraform"
       name:                                       "cloud-db"
       name_prefix:                                <computed>
       subnet_ids.#:                               <computed>
       tags.%:                                     "1"
       tags.Name:                                  "My DB subnet group"
   + aws_instance.jenkins-cloud
       id:                                         <computed>
       ami:                                        "ami-0080e4c5bc078760e"
       arn:                                        <computed>
       associate_public_ip_address:                <computed>
       availability_zone:                          "us-east-1b"
       cpu_core_count:                             <computed>
       cpu_threads_per_core:                       <computed>
       ebs_block_device.#:                         <computed>
       ephemeral_block_device.#:                   <computed>
       get_password_data:                          "false"
       host_id:                                    <computed>
       instance_state:                             <computed>
       instance_type:                              "t2.micro"
       ipv6_address_count:                         <computed>
       ipv6_addresses.#:                           <computed>
       key_name:                                   <computed>
       metadata_options.#:                         <computed>
       network_interface.#:                        <computed>
       network_interface_id:                       <computed>
       password_data:                              <computed>
       placement_group:                            <computed>
       primary_network_interface_id:               <computed>
       private_dns:                                <computed>
       private_ip:                                 <computed>
       public_dns:                                 <computed>
       public_ip:                                  <computed>
       root_block_device.#:                        <computed>
       security_groups.#:                          <computed>
       source_dest_check:                          "true"
       subnet_id:                                  "${aws_subnet.private_b.id}"
       tags.%:                                     "1"
       tags.Name:                                  "Jenkins-Cloud"
       tenancy:                                    <computed>
       volume_tags.%:                              <computed>
       vpc_security_group_ids.#:                   <computed>
   + aws_internet_gateway.gw
       id:                                         <computed>
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Desafio Cloud IGW"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_launch_configuration.this
       id:                                         <computed>
       arn:                                        <computed>
       associate_public_ip_address:                "true"
       ebs_block_device.#:                         <computed>
       ebs_optimized:                              <computed>
       enable_monitoring:                          "true"
       image_id:                                   "ami-0080e4c5bc078760e"
       instance_type:                              "t2.micro"
       key_name:                                   "leandro"
       name:                                       <computed>
       name_prefix:                                "autoscaling-launcher"
       root_block_device.#:                        <computed>
       security_groups.#:                          <computed>
       user_data:                                  "c37dd03619688ec23d81c431e56babc304f62e46"
   + aws_lb.lb
       id:                                         <computed>
       arn:                                        <computed>
       arn_suffix:                                 <computed>
       dns_name:                                   <computed>
       drop_invalid_header_fields:                 "false"
       enable_deletion_protection:                 "false"
       enable_http2:                               "true"
       idle_timeout:                               "60"
       internal:                                   <computed>
       ip_address_type:                            <computed>
       load_balancer_type:                         "application"
       name:                                       "ALB-Desafio-Cloud"
       security_groups.#:                          <computed>
       subnet_mapping.#:                           <computed>
       subnets.#:                                  <computed>
       tags.%:                                     "1"
       tags.Name:                                  "ALB"
       vpc_id:                                     <computed>
       zone_id:                                    <computed>
   + aws_lb_listener.lbl
       id:                                         <computed>
       arn:                                        <computed>
       default_action.#:                           "1"
       default_action.0.order:                     <computed>
       default_action.0.target_group_arn:          "${aws_lb_target_group.tg.arn}"
       default_action.0.type:                      "forward"
       load_balancer_arn:                          "${aws_lb.lb.arn}"
       port:                                       "80"
       protocol:                                   "HTTP"
       ssl_policy:                                 <computed>
   + aws_lb_target_group.tg
       id:                                         <computed>
       arn:                                        <computed>
       arn_suffix:                                 <computed>
       deregistration_delay:                       "300"
       health_check.#:                             "1"
       health_check.0.enabled:                     "true"
       health_check.0.healthy_threshold:           "2"
       health_check.0.interval:                    "30"
       health_check.0.matcher:                     <computed>
       health_check.0.path:                        "/"
       health_check.0.port:                        "traffic-port"
       health_check.0.protocol:                    "HTTP"
       health_check.0.timeout:                     <computed>
       health_check.0.unhealthy_threshold:         "3"
       lambda_multi_value_headers_enabled:         "false"
       load_balancing_algorithm_type:              <computed>
       name:                                       "ALB-TG-Desafio-Cloud"
       port:                                       "80"
       protocol:                                   "HTTP"
       proxy_protocol_v2:                          "false"
       slow_start:                                 "0"
       stickiness.#:                               <computed>
       target_type:                                "instance"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_route_table.rt_private
       id:                                         <computed>
       owner_id:                                   <computed>
       propagating_vgws.#:                         <computed>
       route.#:                                    <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Desafio Cloud route private"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_route_table.rt_public
       id:                                         <computed>
       owner_id:                                   <computed>
       propagating_vgws.#:                         <computed>
       route.#:                                    "1"
       route.~966145399.cidr_block:                "0.0.0.0/0"
       route.~966145399.egress_only_gateway_id:    ""
       route.~966145399.gateway_id:                "${aws_internet_gateway.gw.id}"
       route.~966145399.instance_id:               ""
       route.~966145399.ipv6_cidr_block:           ""
       route.~966145399.nat_gateway_id:            ""
       route.~966145399.network_interface_id:      ""
       route.~966145399.transit_gateway_id:        ""
       route.~966145399.vpc_peering_connection_id: ""
       tags.%:                                     "1"
       tags.Name:                                  "Desafio Cloud route public"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_route_table_association.private_desafio_a
       id:                                         <computed>
       route_table_id:                             "${aws_route_table.rt_private.id}"
       subnet_id:                                  "${aws_subnet.private_a.id}"
   + aws_route_table_association.private_desafio_b
       id:                                         <computed>
       route_table_id:                             "${aws_route_table.rt_private.id}"
       subnet_id:                                  "${aws_subnet.private_b.id}"
   + aws_route_table_association.public_desafio_a
       id:                                         <computed>
       route_table_id:                             "${aws_route_table.rt_public.id}"
       subnet_id:                                  "${aws_subnet.public_a.id}"
   + aws_route_table_association.public_desafio_b
       id:                                         <computed>
       route_table_id:                             "${aws_route_table.rt_public.id}"
       subnet_id:                                  "${aws_subnet.public_b.id}"
   + aws_security_group.alb
       id:                                         <computed>
       arn:                                        <computed>
       description:                                "Load Balancer SG"
       egress.#:                                   "1"
       egress.482069346.cidr_blocks.#:             "1"
       egress.482069346.cidr_blocks.0:             "0.0.0.0/0"
       egress.482069346.description:               ""
       egress.482069346.from_port:                 "0"
       egress.482069346.ipv6_cidr_blocks.#:        "0"
       egress.482069346.prefix_list_ids.#:         "0"
       egress.482069346.protocol:                  "-1"
       egress.482069346.security_groups.#:         "0"
       egress.482069346.self:                      "false"
       egress.482069346.to_port:                   "0"
       ingress.#:                                  "1"
       ingress.2214680975.cidr_blocks.#:           "1"
       ingress.2214680975.cidr_blocks.0:           "0.0.0.0/0"
       ingress.2214680975.description:             ""
       ingress.2214680975.from_port:               "80"
       ingress.2214680975.ipv6_cidr_blocks.#:      "0"
       ingress.2214680975.prefix_list_ids.#:       "0"
       ingress.2214680975.protocol:                "tcp"
       ingress.2214680975.security_groups.#:       "0"
       ingress.2214680975.self:                    "false"
       ingress.2214680975.to_port:                 "80"
       name:                                       "ALB-SG-Desafio-Cloud"
       owner_id:                                   <computed>
       revoke_rules_on_delete:                     "false"
       tags.%:                                     "1"
       tags.Name:                                  "Load Balancer"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_security_group.autoscaling
       id:                                         <computed>
       arn:                                        <computed>
       description:                                "Security group that allows ssh/http and all egress traffic"
       egress.#:                                   "1"
       egress.482069346.cidr_blocks.#:             "1"
       egress.482069346.cidr_blocks.0:             "0.0.0.0/0"
       egress.482069346.description:               ""
       egress.482069346.from_port:                 "0"
       egress.482069346.ipv6_cidr_blocks.#:        "0"
       egress.482069346.prefix_list_ids.#:         "0"
       egress.482069346.protocol:                  "-1"
       egress.482069346.security_groups.#:         "0"
       egress.482069346.self:                      "false"
       egress.482069346.to_port:                   "0"
       ingress.#:                                  "2"
       ingress.2541437006.cidr_blocks.#:           "1"
       ingress.2541437006.cidr_blocks.0:           "0.0.0.0/0"
       ingress.2541437006.description:             ""
       ingress.2541437006.from_port:               "22"
       ingress.2541437006.ipv6_cidr_blocks.#:      "0"
       ingress.2541437006.prefix_list_ids.#:       "0"
       ingress.2541437006.protocol:                "tcp"
       ingress.2541437006.security_groups.#:       "0"
       ingress.2541437006.self:                    "false"
       ingress.2541437006.to_port:                 "22"
       ingress.~1145654509.cidr_blocks.#:          "0"
       ingress.~1145654509.description:            ""
       ingress.~1145654509.from_port:              "80"
       ingress.~1145654509.ipv6_cidr_blocks.#:     "0"
       ingress.~1145654509.prefix_list_ids.#:      "0"
       ingress.~1145654509.protocol:               "tcp"
       ingress.~1145654509.security_groups.#:      <computed>
       ingress.~1145654509.self:                   "false"
       ingress.~1145654509.to_port:                "80"
       name:                                       "autoscaling-desafio-cloud"
       owner_id:                                   <computed>
       revoke_rules_on_delete:                     "false"
       tags.%:                                     "1"
       tags.Name:                                  "Auto Scaling Desafio Cloud"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_security_group.db
       id:                                         <computed>
       arn:                                        <computed>
       description:                                "Allow incoming database connections"
       egress.#:                                   "2"
       egress.2214680975.cidr_blocks.#:            "1"
       egress.2214680975.cidr_blocks.0:            "0.0.0.0/0"
       egress.2214680975.description:              ""
       egress.2214680975.from_port:                "80"
       egress.2214680975.ipv6_cidr_blocks.#:       "0"
       egress.2214680975.prefix_list_ids.#:        "0"
       egress.2214680975.protocol:                 "tcp"
       egress.2214680975.security_groups.#:        "0"
       egress.2214680975.self:                     "false"
       egress.2214680975.to_port:                  "80"
       egress.2617001939.cidr_blocks.#:            "1"
       egress.2617001939.cidr_blocks.0:            "0.0.0.0/0"
       egress.2617001939.description:              ""
       egress.2617001939.from_port:                "443"
       egress.2617001939.ipv6_cidr_blocks.#:       "0"
       egress.2617001939.prefix_list_ids.#:        "0"
       egress.2617001939.protocol:                 "tcp"
       egress.2617001939.security_groups.#:        "0"
       egress.2617001939.self:                     "false"
       egress.2617001939.to_port:                  "443"
       ingress.#:                                  "3"
       ingress.267302002.cidr_blocks.#:            "1"
       ingress.267302002.cidr_blocks.0:            "10.0.0.0/16"
       ingress.267302002.description:              ""
       ingress.267302002.from_port:                "-1"
       ingress.267302002.ipv6_cidr_blocks.#:       "0"
       ingress.267302002.prefix_list_ids.#:        "0"
       ingress.267302002.protocol:                 "icmp"
       ingress.267302002.security_groups.#:        "0"
       ingress.267302002.self:                     "false"
       ingress.267302002.to_port:                  "-1"
       ingress.931551850.cidr_blocks.#:            "1"
       ingress.931551850.cidr_blocks.0:            "10.0.0.0/16"
       ingress.931551850.description:              ""
       ingress.931551850.from_port:                "22"
       ingress.931551850.ipv6_cidr_blocks.#:       "0"
       ingress.931551850.prefix_list_ids.#:        "0"
       ingress.931551850.protocol:                 "tcp"
       ingress.931551850.security_groups.#:        "0"
       ingress.931551850.self:                     "false"
       ingress.931551850.to_port:                  "22"
       ingress.~471043921.cidr_blocks.#:           "0"
       ingress.~471043921.description:             ""
       ingress.~471043921.from_port:               "3306"
       ingress.~471043921.ipv6_cidr_blocks.#:      "0"
       ingress.~471043921.prefix_list_ids.#:       "0"
       ingress.~471043921.protocol:                "tcp"
       ingress.~471043921.security_groups.#:       <computed>
       ingress.~471043921.self:                    "false"
       ingress.~471043921.to_port:                 "3306"
       name:                                       "db"
       owner_id:                                   <computed>
       revoke_rules_on_delete:                     "false"
       tags.%:                                     "1"
       tags.Name:                                  "Database Desafio Cloud"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_security_group.web
       id:                                         <computed>
       arn:                                        <computed>
       description:                                "Allow public inbound traffic"
       egress.#:                                   "1"
       egress.3019821826.cidr_blocks.#:            "1"
       egress.3019821826.cidr_blocks.0:            "10.0.6.0/23"
       egress.3019821826.description:              ""
       egress.3019821826.from_port:                "3306"
       egress.3019821826.ipv6_cidr_blocks.#:       "0"
       egress.3019821826.prefix_list_ids.#:        "0"
       egress.3019821826.protocol:                 "tcp"
       egress.3019821826.security_groups.#:        "0"
       egress.3019821826.self:                     "false"
       egress.3019821826.to_port:                  "3306"
       ingress.#:                                  "3"
       ingress.1799340084.cidr_blocks.#:           "1"
       ingress.1799340084.cidr_blocks.0:           "0.0.0.0/0"
       ingress.1799340084.description:             ""
       ingress.1799340084.from_port:               "-1"
       ingress.1799340084.ipv6_cidr_blocks.#:      "0"
       ingress.1799340084.prefix_list_ids.#:       "0"
       ingress.1799340084.protocol:                "icmp"
       ingress.1799340084.security_groups.#:       "0"
       ingress.1799340084.self:                    "false"
       ingress.1799340084.to_port:                 "-1"
       ingress.2214680975.cidr_blocks.#:           "1"
       ingress.2214680975.cidr_blocks.0:           "0.0.0.0/0"
       ingress.2214680975.description:             ""
       ingress.2214680975.from_port:               "80"
       ingress.2214680975.ipv6_cidr_blocks.#:      "0"
       ingress.2214680975.prefix_list_ids.#:       "0"
       ingress.2214680975.protocol:                "tcp"
       ingress.2214680975.security_groups.#:       "0"
       ingress.2214680975.self:                    "false"
       ingress.2214680975.to_port:                 "80"
       ingress.2617001939.cidr_blocks.#:           "1"
       ingress.2617001939.cidr_blocks.0:           "0.0.0.0/0"
       ingress.2617001939.description:             ""
       ingress.2617001939.from_port:               "443"
       ingress.2617001939.ipv6_cidr_blocks.#:      "0"
       ingress.2617001939.prefix_list_ids.#:       "0"
       ingress.2617001939.protocol:                "tcp"
       ingress.2617001939.security_groups.#:       "0"
       ingress.2617001939.self:                    "false"
       ingress.2617001939.to_port:                 "443"
       name:                                       "web"
       owner_id:                                   <computed>
       revoke_rules_on_delete:                     "false"
       tags.%:                                     "1"
       tags.Name:                                  "Web Server Desafio Cloud"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_subnet.private_a
       id:                                         <computed>
       arn:                                        <computed>
       assign_ipv6_address_on_creation:            "false"
       availability_zone:                          "us-east-1a"
       availability_zone_id:                       <computed>
       cidr_block:                                 "10.0.6.0/23"
       ipv6_cidr_block:                            <computed>
       ipv6_cidr_block_association_id:             <computed>
       map_public_ip_on_launch:                    "false"
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Private Desafio-1a"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_subnet.private_b
       id:                                         <computed>
       arn:                                        <computed>
       assign_ipv6_address_on_creation:            "false"
       availability_zone:                          "us-east-1b"
       availability_zone_id:                       <computed>
       cidr_block:                                 "10.0.4.0/23"
       ipv6_cidr_block:                            <computed>
       ipv6_cidr_block_association_id:             <computed>
       map_public_ip_on_launch:                    "false"
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Private Desafio-1b"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_subnet.public_a
       id:                                         <computed>
       arn:                                        <computed>
       assign_ipv6_address_on_creation:            "false"
       availability_zone:                          "us-east-1a"
       availability_zone_id:                       <computed>
       cidr_block:                                 "10.0.1.0/24"
       ipv6_cidr_block:                            <computed>
       ipv6_cidr_block_association_id:             <computed>
       map_public_ip_on_launch:                    "false"
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Public Desafio-1a"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_subnet.public_b
       id:                                         <computed>
       arn:                                        <computed>
       assign_ipv6_address_on_creation:            "false"
       availability_zone:                          "us-east-1b"
       availability_zone_id:                       <computed>
       cidr_block:                                 "10.0.2.0/24"
       ipv6_cidr_block:                            <computed>
       ipv6_cidr_block_association_id:             <computed>
       map_public_ip_on_launch:                    "false"
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Public Desafio-1b"
       vpc_id:                                     "${aws_vpc.desafio.id}"
   + aws_vpc.desafio
       id:                                         <computed>
       arn:                                        <computed>
       assign_generated_ipv6_cidr_block:           "false"
       cidr_block:                                 "10.0.0.0/16"
       default_network_acl_id:                     <computed>
       default_route_table_id:                     <computed>
       default_security_group_id:                  <computed>
       dhcp_options_id:                            <computed>
       enable_classiclink:                         <computed>
       enable_classiclink_dns_support:             <computed>
       enable_dns_hostnames:                       <computed>
       enable_dns_support:                         "true"
       instance_tenancy:                           "default"
       ipv6_association_id:                        <computed>
       ipv6_cidr_block:                            <computed>
       main_route_table_id:                        <computed>
       owner_id:                                   <computed>
       tags.%:                                     "1"
       tags.Name:                                  "Desafio Cloud VPC"
 Plan: 28 to add, 0 to change, 0 to destroy.
 ------------------------------------------------------------------------
 Note: You didn't specify an "-out" parameter to save this plan, so Terraform
 can't guarantee that exactly these actions will be performed if
 "terraform apply" is subsequently run.
 ```

 3° Para realizara o provisionamento da infraestrutura basta executar um apply:

 ```
 terraform apply -auto-approve
 ```


## INFRAESTRUTURA NA AWS

![Image of Terraform](images/arquitetura_de_infraestrutura.png)



```
resource "aws_security_group" "autoscaling" {
  name        = "autoscaling-desafio-cloud"
  description = "Security group that allows ssh/http and all egress traffic"
  vpc_id      = "${aws_vpc.desafio.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${aws_security_group.alb.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "Auto Scaling Desafio Cloud"
  }
}

resource "aws_launch_configuration" "this" {
  name_prefix                 = "autoscaling-launcher"
  image_id                    = "${var.ami}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_pair}"
  security_groups             = ["${aws_security_group.autoscaling.id}"]
  associate_public_ip_address = true

  user_data = "${file("ec2_setup.sh")}"
}

resource "aws_autoscaling_group" "this" {
  name                      = "cloud-autoscaling"
  vpc_zone_identifier       = ["${aws_subnet.public_a.id}", "${aws_subnet.public_b.id}"]
  launch_configuration      = "${aws_launch_configuration.this.name}"
  min_size                  = 2
  max_size                  = 5
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  target_group_arns         = ["${aws_lb_target_group.tg.arn}"]
  enabled_metrics           = ["${var.enabled_metrics}"]
}

resource "aws_autoscaling_policy" "scaleup" {
  name                   = "Scale Up Desafio Cloud"
  autoscaling_group_name = "${aws_autoscaling_group.this.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_autoscaling_policy" "scaledown" {
  name                   = "Scale Down Desafio Cloud"
  autoscaling_group_name = "${aws_autoscaling_group.this.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_instance" "jenkins-cloud" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"

  vpc_security_group_ids = ["${aws_security_group.db.id}"]
  subnet_id              = "${aws_subnet.private_b.id}"
  availability_zone      = "${var.region}b"

  tags = {
    Name = "Jenkins-Cloud"
  }
}

```